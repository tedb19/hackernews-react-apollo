import React from 'react'
import { Link } from './Link'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'

export const FEED_QUERY = gql`
  {
    feed {
      links {
        id
        url
        description
        createdAt
        postedBy {
          id
          name
        }
        votes {
          id
          user {
            id
          }
        }
      }
    }
  }
`
const updateCacheAfterVote = (store, createVote, linkId) => {
  const data = store.readQuery({ query: FEED_QUERY })
  const votedLink = data.feed.links.find(link => link.id === linkId)
  votedLink.votes = createVote.link.votes
  store.writeQuery({ query: FEED_QUERY, data })
}

export const LinkList = () => (
  <Query query={FEED_QUERY}>
    {({ loading, error, data }) => {
      if (loading) return <div>Fetching...</div>
      if (error)
        return (
          <div>
            Error occured <pre>{error}</pre>
          </div>
        )

      const { links } = data.feed
      return (
        <div>
          {links.map((link, index) => (
            <Link
              key={link.id}
              link={link}
              index={index}
              updateStoreAfterVote={updateCacheAfterVote}
            />
          ))}
        </div>
      )
    }}
  </Query>
)
