import React, { Component } from 'react'
import { Link } from './Link'
import { withApollo } from 'react-apollo'
import gql from 'graphql-tag'

const FEED_SEARCH_QUERY = gql`
  query feedSearchQuery($filter: String!) {
    feed(filter: $filter) {
      links {
        description
        url
        createdAt
        id
        postedBy {
          id
          name
        }
        votes {
          id
          user {
            id
          }
        }
      }
    }
  }
`

class Search extends Component {
  state = {
    filter: '',
    links: []
  }

  _executeSearch = async () => {
    const { filter } = this.state
    const result = await this.props.client.query({
      query: FEED_SEARCH_QUERY,
      variables: { filter }
    })
    const { links } = result.data.feed
    this.setState({ links })
  }

  render() {
    return (
      <div>
        <div>
          Search{' '}
          <input
            type="text"
            onChange={evt => this.setState({ filter: evt.target.value })}
          />
          <button onClick={() => this._executeSearch()}>OK</button>
        </div>
        {this.state.links.map((link, index) => (
          <Link index={index} key={index} link={link} />
        ))}
      </div>
    )
  }
}
/**
 * withApollo injects the ApolloClient instance created in index.js into
 * the Search component as a prop called client
 *
 * This client prop has a function called query that can be used to send a
 * query manually to the graphql server (see the _executeSearch implementation)
 */
export default withApollo(Search)
