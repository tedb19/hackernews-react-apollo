import React from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import { AUTH_TOKEN } from '../constants'

const Header = props => {
  const authToken = localStorage.getItem(AUTH_TOKEN)
  return (
    <div className="flex pal justify-between nowrap orange">
      <div className="flex flex-fixed black">
        <Link to="/" className="ml1 no-underline black">
          <div className="fw7 mr1">Hacker News</div>
        </Link>
        <div className="ml1"> | </div>
        <Link to="/search" className="ml1 no-underline black">
          Search
        </Link>
        {authToken && (
          <div className="flex">
            <div className="ml1"> | </div>
            <Link to="/create" className="ml1 no-underline black">
              Submit
            </Link>
          </div>
        )}
      </div>
      <div className="flex flex-fixed">
        {authToken ? (
          <div
            className="ml1 pointer black"
            onClick={() => {
              localStorage.removeItem(AUTH_TOKEN)
              props.history.push('/') // redirect to home
            }}
          >
            logout
          </div>
        ) : (
          <Link to="/login" className="ml1 no-underline black">
            login
          </Link>
        )}
      </div>
    </div>
  )
}

export default withRouter(Header)
