import React, { Component } from 'react'
import { AUTH_TOKEN } from '../constants'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'

const SIGNUP_MUTATION = gql`
  mutation SignupMutation($email: String!, $name: String!, $password: String!) {
    signup(email: $email, name: $name, password: $password) {
      token
    }
  }
`

const LOGIN_MUTATION = gql`
  mutation LoginMutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
    }
  }
`

class Login extends Component {
  state = {
    login: true, //swithes between login & signup
    email: '',
    password: '',
    name: ''
  }

  _confirm = async data => {
    console.log('data', data)
    const { token } = this.state.login ? data.login : data.signup
    this._saveUserData(token)
    this.props.history.push('/') // redirect to home
  }

  _saveUserData = token => localStorage.setItem(AUTH_TOKEN, token)

  render() {
    const { login, email, password, name } = this.state

    return (
      <div>
        <h4 className="mv3">{login ? 'Login' : 'Sign up'}</h4>
        <div className="flex flex-column">
          {!login && (
            <input
              value={name}
              onChange={evt => this.setState({ name: evt.target.value })}
              type="text"
              placeholder="your name"
            />
          )}
          <input
            value={email}
            onChange={evt => this.setState({ email: evt.target.value })}
            type="text"
            placeholder="your email address"
          />
          <input
            value={password}
            onChange={evt => this.setState({ password: evt.target.value })}
            type="password"
            placeholder="choose a safe password"
          />
        </div>
        <div className="flex mt3">
          <Mutation
            mutation={login ? LOGIN_MUTATION : SIGNUP_MUTATION}
            variables={{ name, email, password }}
            onCompleted={data => this._confirm(data)}
          >
            {mutation => (
              <div className="pointer mr2 button" onClick={mutation}>
                {login ? 'login' : 'create account'}
              </div>
            )}
          </Mutation>
          <div
            className="pointer button"
            onClick={() => this.setState({ login: !login })}
          >
            {login ? 'need to create an account?' : 'already have an account?'}
          </div>
        </div>
      </div>
    )
  }
}

export default Login
